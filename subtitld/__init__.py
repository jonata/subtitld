#######################################################################
#
# Subtitld
#
#######################################################################

__version__ = '23.02.0.0'

__appname__ = 'Subtitld'
__domain__ = 'subtitld.org'
__desktopid__ = 'org.subtitld.Subtitld'
# __appid__ = ''

__author__ = 'Jonatã Bolzan Loss'
__email__ = 'subtitld@subtitld.org'
__website__ = 'https://subtitld.org'
__bugreport__ = 'https://gitlab.com/jonata/subtitld/issues'

__ispypi__ = False
